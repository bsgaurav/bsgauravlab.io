---
title: "Contact"
date: 2023-03-16T21:25:33-04:00
draft: false
---

There are a couple ways to contact. The email will change once i have a personal email server.

<a class="contact-a" href="mail://bs.gaurav@proton.me">E-Mail</a>
<br /><a class="contact-a" href="/mygpg.txt">PGP Key</a>


I'm also going to include addresses here for donations. When you click the link look at the URL, the part after the slash is the Monero address. I didn't write it out because it talkes a lot of space on the page

<a class="contact-a" href="/8BQ3YMHjPz5QG7ZvA2HGSK3WkXrrqHnUSdzPnfTHgSYZ7AdGxq8VLV7CwjSjTN2fMFCFHHtoX2xm83Y7dWM83t5WHJ1nseM">Monero</a>
