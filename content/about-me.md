---
title: "About Me"
date: 2023-03-15T21:38:03-04:00
draft: false
---

Hello, my name is Gaurav. I'll give some vague information about myself and then some more about the website in general.

Well start with how to pronounce my name. Because, oh boy, have I had many "Sorry? ... umm ... yeah ill just call you 'G'" moments. Other nicknames I have gotten include "grr-aw-v" [gɚɑv]. I'm trying to shed myself of that. So, the only nicknames I will accept are: g-rav and gori/gauri.

But, if you know the IPA then this should be really easy for you.

<center>[ˈgʌuɾʌʋ]</center>

If thats genuinely too hard:

<center>[ˈgəuɹəv]</center>

And if thats also hard. Which it shouldn't be, all those sounds are in English as well:

<center>[gɑˈɹəv]</center>

Those are the only three pronunciations I will accept. But, if you just absolutely cannot, say "go" and then "ruv" (like a dog but with a "v" I guess).

Some of my interests include programming. Currently I've only make small scripts and programs. Not anything that i feel is a big project. I like languages too. Be it programming or natural or constructed. Linguistics is fun.

Now, philosophies and beliefs. I wont go in depth with this as I plan on talking more about it and explaining myself in my blog, but that is for a later date. But, simply, I would say I am a minimalist, and an optimistic nihilist. That may seem contradictory (and probably is) but it makes sense to me. So, what more do I need?
