---
title: "projects"
date: 2023-03-28T20:59:24-04:00
draft: false
---

I don't have anything big enough to put here. You can check out my <a href="https://gitlab.com/bsgaurav/postinstall">gitlab</a> for a couple of scripts that I have made. They're all quality of life things that I use on my system.

Cool programs so far:
1. aurh
	- A very simple AUR helper for Arch based linux distributions (distros).
	- It can search for packages, as well as instal, update (manually), and remove.
    - Currently being worked on

2. bookmark & getbookmark
	- These programs work together to create a system-wide bookmark manager.
	- The bookmark program adds selected text to a file you specify and the getbookmark file outputs the file to a menu program and types the selected line.
	- They are seperated like this because I use these programs to also retrieve my terminal history.

3. minddit & mindditpost
	- Get it? "Minimal-Reddit." It's a terminal Reddit viewer. You specify a subreddit and some more information and it gets the post. You can view images, videos, and text posts.
	- It is integrated with mindditpost so it can specific posts and view the comments.

4. pash & pashmenu
	- I didn't make the pash program. It is a project started by <a href="https://github.com/dylanaraps/pash">dylanaraps</a> and is simply a posix compliant version of pass - the terminal based password manager for Linux.
	- What I did do though is add in support for One Time Passwords (OTPs) and create pashmenu to use pash without opening a terminal. It is a bit hacky, though.
	- I haven't opened a pull request for it yet because I don't want to make a GitHub account.

5. termtube
	- It is a simple terminal based youtube frontend using mpv.
	- It currently uses dmenu as the menu system but can easily be changed for fzf or something else.

6. walgen
	- It's my script to manage my wallpaper and theme of my desktop.
	- It can download random wallpapers, or based on a search query, or just simply use pictures you already have on your system.
	- Then it uses pywal to generate a colorscheme and applies it to the programs that I want to theme.

7. <a href="https://gitlab.com/bsgaurav/dodo">dodo</a>
    - a super minimalist todo manager

As I make more things and things that I'm more proud of, I'll share the here.
