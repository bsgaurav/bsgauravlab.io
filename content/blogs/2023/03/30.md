---
title: "Apple Slander"
date: 2023-03-30T19:07:36-04:00
draft: false
---

TL;DR Apple is a bad company, check out the Fairphone and Lightphone.

Apple is absolutely one of the worst phones and companies in the world. There is the basic stuff: Oh they make terrible phones, they own sweatshops in China! Oh no! Yada yada.

But, there is also the absolute lack of self awareness. Yes it is terrible that they own sweat shops, but so does every other company. They're overpriced, but so are eggs. I had the idea to write this blog when I was doing an assignment for school. It was an anthropology assignment on globalization. It was specifically about the terrible working conditions in Apple's factories in China. From what I could tell from the article provided to me, Apple apparently does know about the problems, and they allegedly try to implement changes, but its pretty much a corporate game of telephone. But instead of mishearing, one of the players is the annoying kid that changes the phrase on purpose.

Anyway, moving on to Apple's lack of self awareness. They do this all of the time, when adding new features, when taking away features they deem unnecesary. Whatever it may be, they spin it to make it look good on them. One example being when they removed the charger head from iPhone boxes. No matter how much they publicaly announce that they're doing something for the environment or for something in that vein. There is someone else doing more and doing it better. Removing a charger head does do something in reducing electronic waste. True. But you know what would do more? Not releasing a new iPhone every year. Maybe spend more time on making a good, new phone?

It seems like releasing a new phone, and making older (not even old) phones obselete is higher on their To Do list than actually making changes to their business model.

They used to be a somewhat decent company too. The privacy they used to provide to users by default used to be amazing, much better than any basic Android phone. Even if their products are more restricted. But then they "realized" that kids also used their products, and so did pedophiles. So "for the children" they have backdoors in their products, that they can use to access your files, photos, messages, everything.

iMessage used to be a decent messaging app. But once you have a back door, it doesn't matter how many ways a conversation is encrypted, there is always a way for someone to access your data.

Obviously, I also care about the children. But sacrificing everyone's privacy to catch a small percentage of your users that are pedos is not a good model. There are people streaming on Twitch catfishing pedos. There are better ways to handle these problems.

On my point of someone doing everything Apple is doing but better: take a company like FairPhone. They're an electronics manufacturer based in the Netherlands and they provide mid-range Android phones. For about 500 Euros, you can get a phone with decent specs. Now its not gonna be no iPhone, or Samsung, or something. It's a midrange phone. You're gonna get what you paid for. But in some ways you will get more than you paid for.

The problem with sweatshops? They pay fair wages to the people that manufacture their phones.

The environment? They package their phones in recycled cardboard boxes. They even go as far as to print the words on their boxes using soy ink!? What's better is that they provide seven. Yes, seven (7) **years** of software updates. Their phones are almost entirely modular. Camera broken? Don't buy a new phone. Buy the camera, and replace it yourself! Look up some videos on Youtube about the Fairphone. They are insanely manipulatable.

Privacy? They *encourage* people who buy their phones to install their own operating system, and make it easy to do so.

Bad messenger? Use Signal. They are an amazing company that truely believe in user privacy.

Lack of features? Its an Android. You can strip it bare, or dress it like a queen. You can make it whatever you want it to be. This is especially appealing to someone like me. I love minimalism. And recently, I've been minimising my phone usage by removing social media accounts, moving my mail to be solely on my laptop. And more. I'll talk about that in another blog post. But what I don't like about the iPhone (Yes, I own an iPhone. I'm being a hippocrite.) is that there is no way to customise the way apps look, or the way the home screen and lock screen look without jailbreaking it. I would like to have a minimalist launcher that looks like the Lightphone 2. But I cannot do that. But on an android. I just download a simple launcher.

Whatever.

Linus Tech Tips on <a href="https://yewtu.be/watch?v=L1v3ZmfV_hw">Fairphone</a>  
Marques Brownlee on <a href="https://yewtu.be/watch?v=gkmzDwgvqQM">Fairphone</a>  
Mental Outlaw on <a href="https://yewtu.be/watch?v=3oPeIbpA5x8">Signal</a>  
Mental Outlaw on <a href="https://yewtu.be/watch?v=_YQEE9Jl7fs">Apple</a>  
Mental Outlaw on <a href="https://yewtu.be/watch?v=YgasaIly6Eo">Apple</a>  
